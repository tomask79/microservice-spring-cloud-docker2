package com.example.client;

import com.example.client.feign.SimpleMicroServiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tomask79 on 06.11.16.
 */
@Component
public class MicroServiceInvoker {

    @Autowired
    private SimpleMicroServiceFeignClient simpleMicroServiceFeignClient;

    public void invokeMicroService() {
        final String invokeResult = simpleMicroServiceFeignClient.
                invokePersonsMicroService();
        System.out.println("*************************************************************");
        System.out.println("Simple MicroService Feign invocation result :"+invokeResult);
        System.out.println("*************************************************************");
    }
}
