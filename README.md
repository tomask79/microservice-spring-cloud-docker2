# Writing MicroServices [part 5] #

## Spring Cloud MicroServices with Docker ##

Okay, let's show another way of how to dockerize your Spring Cloud NetFlix app.
Imagine that you're building a [MicroService Gateway](http://microservices.io/patterns/apigateway.html), in the Docker language you're going to be building a container application that needs to be able to see eureka server and MicroServices as well. Perfect solution for this is to use [Docker Links](https://docs.docker.com/engine/userguide/networking/default_network/dockerlinks/)!

### Eureka Server ###

application.properties
```
.
.
eureka.instance.prefer-ip-address=false
eureka.instance.hostname=eureka
```
With this settings, Eureka server registry will be downloadable for clients at address http://eureka:<port>/eureka 

*Starting the Eureka registry in Docker*
```
(in the spring-microservice-registry folder)
mvn clean install docker:build
docker images (verify your image was built)
docker run -it -p 9761:9761 --name eureka registry/demo
```
I do the ports mapping only because I want to use eureka web page. What's **important is using the container name**.

### MicroService ###

Lets set previously configured Eureka as registry source for MicroService:

```
spring.application.name=personsService
eureka.client.serviceUrl.defaultZone=http://eureka:9761/eureka
eureka.client.healthcheck.enabled=true

eureka.instance.preferIpAddress=false
eureka.instance.hostname=service

ribbon.eureka.enabled=true
```

*Starting the MicroService in Docker*
```
(in the spring-microservice-service folder)
mvn clean install -DskipTests docker:build
docker images (verify your image was built)
docker run -it --link eureka:eureka --name service service/demo
docker ps (verify that service container is running)
```
With "docker run" like this *service/demo* container is going to see Eureka server under the alias eureka, let's verify that (run the following):
```
docker exec -it service bash (this will connect you to the MicroService cont.)
ping eureka
```
Output should look like:
```
root@27bf836030fb:/# ping eureka
PING eureka (172.17.0.2): 56 data bytes
64 bytes from 172.17.0.2: icmp_seq=0 ttl=64 time=0.269 ms
64 bytes from 172.17.0.2: icmp_seq=1 ttl=64 time=0.084 ms
```

### Gateway container ###

Gateway container is going to need to be linked to eureka and service container as well, because eureka will give ribbon MicroService address in the form http://service:8080. So the gateway will need to understand what "service" means.

application.properties
```
server.port=8888

hystrix.command.invokePersonsMicroService.fallback.enabled=true
hystrix.command.invokePersonsMicroService.metrics.rollingStats.timeInMilliseconds=35000
hystrix.command.invokePersonsMicroService.circuitBreaker.sleepWindowInMilliseconds=5000
hystrix.command.invokePersonsMicroService.circuitBreaker.requestVolumeThreshold=6
hystrix.command.invokePersonsMicroService.circuitBreaker.errorThresholdPercentage=100
hystrix.command.invokePersonsMicroService.execution.isolation.strategy=THREAD

eureka.client.serviceUrl.defaultZone=http://eureka:9761/eureka

eureka.client.healthcheck.enabled=true
eureka.client.registryFetchIntervalSeconds=5
eureka.client.fetch-registry=true
```

*Running the gateway in Docker*

```
(in the spring-microservice-client folder)
mvn clean install -DskipTests docker:build
docker images (verify that image was built)
docker run -it --link eureka:eureka --link service:service --name client client/demo
```
after running the image, you should see again repeated output:

```
Simple MicroService Feign invocation result :{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}
```

### Summary ###
Solution via Docker links is popular. But you often need to link the containers in both directions, that they need to see at each other. Docker links then won't work and you need to use [Docker network](https://docs.docker.com/engine/userguide/networking/).

see ya

Tomas